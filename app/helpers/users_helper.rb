module UsersHelper
  def field_options_for_date_of_birth
    current_year = Time.zone.today.year
    {
      as: :date,
      include_blank: true,
      start_year: current_year - 100,
      end_year: current_year - 12,
      order: %i[day month year]
    }
  end

  def users_avatar_img(user)
    image_tag users_avatar_src(user),
      alt: user.visible_name,
      size: '80x80',
      class: 'img-circle'
  end

  private

  def users_avatar_src(user)
    if user.avatar?
      user.avatar_url
    elsif !user.local?
      user.last_auth_token.avatar_url
    else
      'default-avatar.jpg'
    end
  end
end
