module TestsHelper
  include ActsAsTaggableOn::TagsHelper

  def options_for_select_test_scope
    Test.scopes.keys.map { |k| [t("tests.form.scope.#{k}"), k] }
  end
end
