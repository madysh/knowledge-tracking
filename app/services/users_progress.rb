class UsersProgress
  def initialize(user_id)
    @user_id = user_id
  end

  def count_of_tests
    @count_of_tests ||= Test.for_everyone.where.not(user_id: @user_id).count
  end

  def not_started_tests
    if count_of_tests.zero? || count_of_tests == completed_tests.count
      []
    elsif @not_started_tests
      @not_started_tests
    else
      @not_started_tests = transfer_to_hash(not_started_tests_scope)
    end
  end

  def completed_tests
    if count_of_tests.zero?
      []
    else
      @completed_tests ||= transfer_to_hash(completed_tests_records)
    end
  end

  def average_value_of_answers
    if count_of_tests.zero? || count_of_tests == not_started_tests.count
      0
    elsif @average_value_of_answers
      @average_value_of_answers
    else
      counts = completed_tests_records.map { |v| v['correct_answers_count'] }
      @average_value_of_answers = counts.inject(&:+).to_f / counts.size
    end
  end

  private

  def all_tests_scope
    return @all_tests_scope if @all_tests_scope

    scope = Category.all
    scope = add_joins_scopes(scope)
    scope = add_where_scopes(scope)
    scope = add_select(scope)
    @all_tests_scope = scope.group('topics.id, tests.id')
  end

  def add_joins_scopes(scope)
    scope
      .joins(%i[tests topics])
      .joins('LEFT JOIN test_results ON test_results.test_id = tests.id')
  end

  def add_where_scopes(scope)
    scope
      .where('tests.scope': Test.scopes[:for_everyone])
      .where('tests.user_id != ?', @user_id)
  end

  def add_select(scope)
    scope.select(
      'categories.id as cat_id',
      'categories.name as cat_name',
      'topics.id as topic_id',
      'topics.name as topic_name',
      'tests.id as test_id',
      'tests.title as test_title',
      'test_results.correct_answers_count'
    )
  end

  def not_started_tests_scope
    all_tests_scope
      .where(
        'test_results.user_id IS NULL OR test_results.user_id != ?', @user_id
      )
      .where.not('tests.id': completed_tests_ids)
  end

  def completed_tests_records
    @completed_tests_records ||= all_tests_scope
      .where('test_results.state = ?', TestResult.states[:active])
      .where('test_results.user_id = ?', @user_id)
  end

  def completed_tests_ids
    completed_tests_records.map { |v| v['test_id'] }
  end

  def transfer_to_hash(data)
    data
      .as_json
      .group_by { |item| [item['cat_id'], item['cat_name']] }
      .transform_values do |v|
        v.group_by { |item| [item['topic_id'], item['topic_name']] }
      end
  end
end
