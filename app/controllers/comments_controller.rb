class CommentsController < ApplicationController
  respond_to :json, only: :create

  def create
    @test = Test.find(params[:test_id])
    @level = params[:comment][:level].to_i
    if params[:comment_id]
      define_comment_by_comment_id
    else
      define_comment
    end
  end

  private

  def define_comment_by_comment_id
    @parent_comment = Comment.find(params[:comment_id])
    @comment = @parent_comment
      .comments
      .where(user: current_user)
      .create(comment_params)
  end

  def define_comment
    @comment = @test.comments.where(user: current_user).create(comment_params)
  end

  def comment_params
    params.require(:comment).permit(:body)
  end
end
