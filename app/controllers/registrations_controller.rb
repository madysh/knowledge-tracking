class RegistrationsController < Devise::RegistrationsController
  before_action :configure_permitted_parameters

  private

  def after_inactive_sign_up_path_for(*)
    new_user_session_path
  end

  def configure_permitted_parameters
    users_attrs = %i[
      first_name last_name nickname email date_of_birth address system_language
      password password_confirmation current_password avatar
    ]
    devise_parameter_sanitizer.permit :sign_up, keys: users_attrs
    devise_parameter_sanitizer.permit :account_update, keys: users_attrs
  end

  def update_resource(resource, params)
    if local_or_passed_password?(params)
      resource.update_with_password(params)
    else
      resource.update_without_password(params)
    end
  end

  def local_or_passed_password?(params)
    current_user.local? ||
      params['password'].present? ||
      params['password_confirmation'].present?
  end
end
