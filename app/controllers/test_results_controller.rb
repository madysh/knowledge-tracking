class TestResultsController < ApplicationController
  before_action :set_test, only: %i[new create]

  def index
    @progress = UsersProgress.new(current_user.id)
  end

  def new
    @test_result = current_user.test_results.where(test: @test).build
  end

  def create
    @test_result = current_user.test_results.build(test_result_params)
    if @test_result.save
      @rating = current_user.ratings.where(test: @test).first_or_initialize
      render :congratulations
    else
      render :new
    end
  end

  private

  def set_test
    @test = Test.for_everyone.find(params[:test_id])
  end

  def test_result_params
    answers_keys = (1..Test::QUESTIONS_COUNT).map(&:to_s)
    params
      .require(:test_result)
      .permit(:question_id, :test_id, answers: answers_keys)
  end
end
