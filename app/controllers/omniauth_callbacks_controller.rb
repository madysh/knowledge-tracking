class OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def auth_provider
    auth_token = AuthToken.find_using_omniauth(auth_hash)
    if auth_token&.user
      user = auth_token.user
    else
      user = User.find_or_create_by_omniauth(auth_hash)
      AuthToken.create_or_update_from_omniauth(auth_hash, user)
    end

    sign_in user
    redirect_to root_path
  end

  alias facebook auth_provider
  alias linkedin auth_provider
  alias twitter auth_provider

  private

  def auth_hash
    request.env['omniauth.auth']
  end
end
