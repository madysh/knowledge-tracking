class TestsController < ApplicationController
  before_action :set_test, only: %i[edit update destroy]

  def index
    @tests = current_user
      .tests
      .includes(:topic)
      .page(params[:page])
      .order(created_at: :desc)
  end

  def show
    @test = test_for_show_action
    @users_rating = current_user.ratings.where(test: @test).first_or_initialize
    @comment = @test.comments.where(user: current_user).build
    @comments = @test.comments.includes(:user, :comments)
  end

  def new
    @test = current_user.tests.build
  end

  def create
    @test = current_user.tests.build(test_params)

    if @test.save
      redirect_to tests_path
    else
      render :new
    end
  end

  def update
    if @test.update(test_params)
      redirect_to @test
    else
      render :edit
    end
  end

  def destroy
    @test.destroy
    redirect_to tests_path
  end

  private

  def test_for_show_action
    current_user.tests.includes(:topic).where(id: params[:id]).first ||
      Test.for_everyone.includes(:topic).find(params[:id])
  end

  def set_test
    @test = current_user.tests.includes(:topic).find(params[:id])
  end

  def test_params
    params.require(:test).permit(
      :tag_list, :topic_id, :scope, :title, :description,
      questions: [:body, :correct_answer, answers: [:body]]
    )
  end
end
