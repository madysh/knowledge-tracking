class HomeController < ApplicationController
  def index
    @tests = tests_for_index_page
    @tags = Test.for_everyone.tag_counts_on(:tags)
  end

  private

  def tests_for_index_page
    if params[:tag]
      all_tests_for_everyone.tagged_with(params[:tag])
    else
      all_tests_for_everyone
    end
  end

  def all_tests_for_everyone
    Test
      .for_everyone
      .includes(:topic)
      .page(params[:page])
      .order(average_rating: :desc, created_at: :desc)
  end
end
