class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :authenticate_user!
  layout :layout_by_resource
  protect_from_forgery with: :exception
  before_action :set_users_language

  private

  def set_users_language
    I18n.locale = current_user.system_language if signed_in?
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit :sign_in,
      keys: %i[email password remember_me]
  end

  def layout_by_resource
    devise_controller? ? 'simple' : 'application'
  end

  def authenticate_admin_user!
    redirect_to(root_path) unless current_user.try(:admin?)
  end

  def after_sign_out_path_for(*)
    new_user_session_path
  end
end
