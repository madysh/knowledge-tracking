class RatingsController < ApplicationController
  before_action :define_test
  before_action :find_rating, only: %i[update destroy]
  after_action :find_rating, only: %i[update destroy]
  respond_to :json

  def create
    @rating = rating_sope.create(score: params[:score])
    @test.recount_average_rating
    render :update_ratings
  end

  def update
    @rating.score = params[:score]
    @rating.save
    @test.recount_average_rating
    render :update_ratings
  end

  def destroy
    @rating.destroy
    @test.recount_average_rating
    render :update_ratings
  end

  private

  def define_test
    @test = Test.find(params[:test_id])
  end

  def rating_sope
    current_user.ratings.where(test: @test)
  end

  def find_rating
    @rating = rating_sope.first
  end
end
