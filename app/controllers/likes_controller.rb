class LikesController < ApplicationController
  respond_to :json
  before_action :define_test

  def like
    current_user.like!(@test)
    reload_test_and_render_view
  end

  def unlike
    current_user.unlike!(@test)
    reload_test_and_render_view
  end

  private

  def reload_test_and_render_view
    @test.reload
    render :update
  end

  def define_test
    @test = Test.find(params[:test_id])
  end
end
