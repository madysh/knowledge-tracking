$ ->
  $('.average-rating').raty
    readOnly: true,
    halfShow: true,
    path: '/assets/ratings',
    score: $(this).data('score')

  $('.users-rating').raty
    path: '/assets/ratings',
    cancel: true,
    cancelPlace: 'right',
    score: $(this).data('score'),

    click: (score, event) ->
      testId = $(this).data('test-id')
      ratingId = $(this).attr('data-rating-id')
      if (score == null && ratingId == undefined)
        return false

      url = '/tests/' + testId + '/ratings'
      if ratingId
        type = if (score == null) then 'DELETE' else 'PATCH'
        url += '/' + ratingId
      else
        type = 'POST'

      $.ajax
        url: url,
        type: type,
        data: { score: score }
        success: ->
          if score == null
            $('.users-rating').removeAttr('data-rating-id')
