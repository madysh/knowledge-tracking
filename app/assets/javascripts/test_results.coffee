$ ->
  $form = $('#new_test_result')
  $form.find('.nav-tabs li').slice(2).find('a').addClass('disabled')
  questionsCount = 5

  nextTab = ->
    $form.find('.nav-tabs > .active').next('li').find('a')

  activateNextTab = ->
    nextTab().removeClass('disabled')

  enableButton = (pane, selector) ->
    pane.find(selector).removeClass('disabled').attr('disabled', null)

  completedTestsCount = ->
    $form.find('.radio-buttons input:checked').length

  updateProgressBar = ->
    $progressBar = $('.progress-bar')
    valuenow = completedTestsCount()
    width = (valuenow*100)/questionsCount
    $progressBar.css('width', width+'%').attr('aria-valuenow', valuenow);
    $progressBar.find('.answered-tests-count').html(valuenow)


  $form.on 'click', '.btnNext', (e) ->
    e.preventDefault()
    activateNextTab()
    nextTab().tab('show')
    false

  $form.on 'click', '.btnPrevious', (e) ->
    e.preventDefault()
    prevTab = $('.nav-tabs > .active').prev('li').find('a')
    prevTab.tab('show')
    false

  $form.on 'click', '.disabled', (e) ->
    e.preventDefault()
    false

  $form.on 'click', 'input[type=radio]', (e) ->
    pane = $(e.target).closest('.tab-pane')
    activateNextTab()
    enableButton(pane, '.btnNext')
    enableButton(pane, 'input[type=submit]')
    updateProgressBar()
