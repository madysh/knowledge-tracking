$ ->
  $container = $('.like-unlike-buttons')

  $container.on 'click', '.btn', ->
    testId = $container.data('test-id')
    url = '/tests/' + testId + '/'
    if $(@).hasClass('like-btn')
      url += 'like'
    else
      url += 'unlike'

    $.ajax
      url: url,
      type: 'GET',
      success: ->
        $container.find('.btn').toggleClass('hidden')

