class Category < ApplicationRecord
  auto_strip_attributes :name
  acts_as_taggable

  has_many :topics, dependent: :destroy
  has_many :tests, through: :topics, dependent: :destroy

  validates :name, presence: true, uniqueness: true

  def self.names_with_topics
    all.includes(:topics).map do |cat|
      topics = cat.topics.pluck(:name, :id)
      [cat.name, topics]
    end
  end
end
