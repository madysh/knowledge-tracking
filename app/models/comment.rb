class Comment < ApplicationRecord
  include ActsAsCommentable::Comment

  MAX_LENGTH = 256
  MAX_NESTING_LEVEL = 3

  has_and_belongs_to_many :commentable, polymorphic: true
  belongs_to :user

  auto_strip_attributes :body
  acts_as_commentable

  validates :user_id, :commentable_type, :commentable_id,
    presence: true

  validates :body, presence: true, length: { maximum: MAX_LENGTH }
end
