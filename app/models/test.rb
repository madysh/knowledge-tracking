class Test < ApplicationRecord
  QUESTIONS_COUNT = 5
  ANSWERS_COUNT = 3

  serialize :questions, JSON

  acts_as_likeable
  acts_as_commentable
  acts_as_taggable

  auto_strip_attributes :title, :description

  enum scope: %i[for_everyone only_for_me]

  belongs_to :topic
  belongs_to :user

  has_many :ratings, dependent: :destroy

  validates :title, :description, :user_id, :topic_id, :questions, :scope,
    presence: true

  after_update :recount_average_rating, if: :ratings_count_changed?
  before_create :add_tags_from_topic_and_category

  def recount_average_rating
    self.average_rating = actual_average_rating
    save
  end

  private

  def actual_average_rating
    if reload.ratings_count.zero?
      0
    else
      ratings_sum = ratings.pluck(:score).inject(&:+)
      ratings_sum.to_f / ratings_count
    end
  end

  def add_tags_from_topic_and_category
    self.tag_list += topic.tag_list
    self.tag_list += topic.category.tag_list
  end
end
