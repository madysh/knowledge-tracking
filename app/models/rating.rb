class Rating < ApplicationRecord
  belongs_to :test, counter_cache: :ratings_count
  belongs_to :user
end
