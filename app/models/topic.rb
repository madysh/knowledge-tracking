class Topic < ApplicationRecord
  auto_strip_attributes :name
  acts_as_taggable

  belongs_to :category
  has_many :topics, dependent: :destroy
  has_many :tests, dependent: :destroy

  validates :name, presence: true
end
