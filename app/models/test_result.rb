class TestResult < ApplicationRecord
  serialize :answers, JSON

  enum state: %i[active archived]

  belongs_to :test
  belongs_to :user

  validates :user_id, :test_id, :answers, :correct_answers_count, :state,
    presence: true

  before_validation :count_correct_answers_count
  before_create :archive_previous

  private

  def count_correct_answers_count
    questions = test.try(:questions)
    return unless questions

    self.correct_answers_count = answers.inject(0) do |count, (k, v)|
      if questions[k]['correct_answer'] == v
        count + 1
      else
        count
      end
    end
  end

  def archive_previous
    previous = user.test_results.where(test_id: test_id).active
    previous.update_all(state: self.class.states[:archived])
  end
end
