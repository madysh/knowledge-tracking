class AuthToken < ApplicationRecord
  belongs_to :user

  enum provider: %i[facebook linkedin twitter]

  validates :uid, :provider, :token, :user_id, presence: true

  def self.find_using_omniauth(auth_params)
    where(uid: auth_params.uid, provider: auth_params.provider).last
  end

  def self.create_or_update_from_omniauth(auth_params, user)
    where(user: user, provider: auth_params.provider)
      .first_or_initialize do |auth_token|

      auth_token.uid = auth_params.uid
      auth_token.token = auth_params.credentials.token
      auth_token.avatar_url = auth_params.info.image
      auth_token.save
    end
  end
end
