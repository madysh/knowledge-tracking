class User < ApplicationRecord
  ALLOWED_SYSTEM_LANGUAGES = %w[en ru].freeze
  DEFAULT_SYSTEM_LANGUAGE = :en
  MAX_LENGTH = {
    name: 64,
    password: 64,
    email: 64
  }.freeze
  MIN_LENGTH = { password: 6 }.freeze
  ONLY_LETTERS_AND_SPACE_REGEX = /\A[\p{L} ]+\z/

  has_many :auth_tokens, dependent: :destroy
  has_many :tests, dependent: :destroy
  has_many :test_results, dependent: :destroy
  has_many :ratings, dependent: :destroy
  has_many :comments, dependent: :destroy

  enum provider: %i[local facebook linkedin twitter]
  enum role: %i[user admin]

  acts_as_liker

  devise :database_authenticatable, :registerable, :rememberable,
    :trackable, :omniauthable, :confirmable, :recoverable,
    omniauth_providers: %i[facebook linkedin twitter]

  auto_strip_attributes :first_name, :last_name, :email, :nickname

  validates :password,
    confirmation: true,
    length: { minimum: MIN_LENGTH[:password], maximum: MAX_LENGTH[:password] },
    allow_blank: true
  validates :password, presence: true, on: :create
  validates :first_name, presence: true
  validates :first_name, :last_name,
    length: { maximum: MAX_LENGTH[:name] },
    format: { with: ONLY_LETTERS_AND_SPACE_REGEX },
    allow_blank: true
  validates :email,
    uniqueness: true,
    length: { maximum: MAX_LENGTH[:email] },
    format: { with: Devise.email_regexp },
    allow_blank: true
  validates :email, presence: { unless: ->(user) { user.twitter? } }
  validates :nickname,
    uniqueness: true,
    length: { maximum: MAX_LENGTH[:name] },
    allow_blank: true
  validates :system_language, inclusion: { in: ALLOWED_SYSTEM_LANGUAGES }
  validates :provider, :role, presence: true

  mount_uploader :avatar, AvatarUploader
  serialize :avatars, JSON

  def visible_name
    nickname || [first_name, last_name].join(' ')
  end

  def owns_test?(test)
    test.user_id == id
  end

  def last_auth_token
    auth_tokens.order(updated_at: :desc).first
  end

  def self.find_or_create_by_omniauth(auth_hash)
    if auth_hash.provider == 'twitter'
      find_or_create_by_twitter_account(auth_hash)
    else
      find_or_create_by_oauth_auth_hash(auth_hash)
    end
  end

  def self.find_or_create_by_oauth_auth_hash(auth_hash)
    where(email: auth_hash.info.email).first_or_create do |user|
      user.first_name = auth_hash.info.first_name
      user.use_attrs_from_auth_hash(auth_hash)
    end
  end

  def self.find_or_create_by_twitter_account(auth_hash)
    where(nickname: auth_hash.info.nickname).first_or_create do |user|
      user.first_name = auth_hash.info.name
      user.use_attrs_from_auth_hash(auth_hash)
    end
  end

  def use_attrs_from_auth_hash(auth_hash)
    self.last_name = auth_hash.info.last_name
    self.password = Devise.friendly_token
    self.password_confirmation = password
    self.provider = auth_hash.provider
    skip_confirmation!
  end
end
