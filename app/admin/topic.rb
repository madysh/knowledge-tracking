ActiveAdmin.register Topic do
  permit_params :name, :category_id, :tag_list

  index do
    selectable_column
    id_column
    column :name
    column :category, ->(topic) { topic.category.name }
    column :created_at
    actions
  end

  filter :name
  filter :created_at
  filter :category_id,
    as: :select,
    collection: -> { Category.pluck(:name, :id) }

  form do |f|
    f.inputs do
      f.input :name
      f.input :category_id,
        as: :select,
        collection: Category.pluck(:name, :id),
        include_blank: false
      f.input :tag_list
    end
    f.actions
  end
end
