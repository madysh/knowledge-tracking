ActiveAdmin.register TestResult do
  controller do
    actions :all, except: %i[edit]
  end

  index do
    selectable_column
    id_column
    column :user_id
    column :test_id
    column :category, ->(test_result) { test_result.test.topic.category.name }
    column :topic, ->(test_result) { test_result.test.topic.name }
    column :created_at
    column :archived_at
    actions
  end

  filter :user_id
  filter :test_id
  filter :created_at
end
