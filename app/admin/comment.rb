ActiveAdmin.register Comment do
  permit_params :body, :user_id, :commentable_id

  index do
    selectable_column
    id_column
    column :user_id
    column :body
    column :commentable_id
    column :created_at
    actions
  end

  filter :user_id
  filter :commentable_id
  filter :created_at

  form do |f|
    f.inputs do
      f.input :user_id
      f.input :commentable_id
      f.input :body
    end
    f.actions
  end
end
