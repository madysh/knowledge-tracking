ActiveAdmin.register Test do
  permit_params :title, :user_id, :scope, :topic_id, :body, :questions

  index do
    selectable_column
    id_column
    column :user_id
    column :title
    column :category, ->(test) { test.topic.category.name }
    column :topic, ->(test) { test.topic.name }
    column :scope
    column :created_at
    actions
  end

  filter :title
  filter :user_id
  filter :scope, as: :select, collection: -> { Test.scopes }
  filter :created_at
  filter :topic_id,
    as: :select,
    collection: -> do
      Category.all.map { |cat| [cat.name, cat.topics.pluck(:name, :id)] }
    end

  form do |f|
    f.inputs do
      f.input :title
      f.input :topic_id,
        as: :select,
        include_blank: false,
        collection: Category.names_with_topics
      f.input :scope, include_blank: false
      f.input :description
    end
    f.actions
  end
end
