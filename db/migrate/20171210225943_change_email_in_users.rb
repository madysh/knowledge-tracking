class ChangeEmailInUsers < ActiveRecord::Migration[5.1]
  def up
    change_column :users, :email, :string,
      unique: true, null: true, default: nil
  end

  def down
    change_column :users, :email, :string, null: false, default: ''
  end
end
