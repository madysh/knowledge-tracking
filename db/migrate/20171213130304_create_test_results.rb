class CreateTestResults < ActiveRecord::Migration[5.1]
  def change
    create_table :test_results do |t|
      t.references :user, null: false, index: true
      t.references :test, null: false, index: true
      t.text :answers, null: false
      t.integer :correct_answers_count, limit: 1, null: false
      t.timestamps
    end
  end
end
