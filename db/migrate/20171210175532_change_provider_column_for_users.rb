class ChangeProviderColumnForUsers < ActiveRecord::Migration[5.1]
  def up
    change_column :users, :provider, :tinyint, null: false, default: 0
  end

  def down
    change_column :users, :provider, :string
  end
end
