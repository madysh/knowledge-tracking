class CreateTests < ActiveRecord::Migration[5.1]
  def change
    create_table :tests do |t|
      t.references :user, null: false, index: true
      t.string :title, null: false
      t.string :description, null: false
      t.references :topic, null: false, index: true
      t.integer :scope, null: false, default: 0, limit: 1
      t.text :questions, null: false
      t.timestamps
    end
  end
end
