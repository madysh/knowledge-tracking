class AddRatingsCountToTests < ActiveRecord::Migration[5.1]
  def change
    add_column :tests, :ratings_count, :integer, null: false, default: 0
  end
end
