class ChangeProviderColumnForAuthTokens < ActiveRecord::Migration[5.1]
  def up
    change_column :auth_tokens, :provider, :tinyint, null: false
  end

  def down
    change_column :auth_tokens, :provider, :string
  end
end
