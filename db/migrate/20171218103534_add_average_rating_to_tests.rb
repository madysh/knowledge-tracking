class AddAverageRatingToTests < ActiveRecord::Migration[5.1]
  def change
    add_column :tests, :average_rating, :float, precision: 1
  end
end
