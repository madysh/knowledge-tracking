class CreateComments < ActiveRecord::Migration[5.1]
  def up
    create_table :comments do |t|
      t.text :body
      t.references :commentable, polymorphic: true
      t.references :user
      t.timestamps
    end

    add_index :comments, :commentable_type
    add_index :comments, :commentable_id
  end

  def down
    drop_table :comments
  end
end
