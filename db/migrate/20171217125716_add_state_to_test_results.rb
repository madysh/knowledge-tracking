class AddStateToTestResults < ActiveRecord::Migration[5.1]
  def change
    add_column :test_results, :state, :integer,
      null: false, default: 0, limit: 1
  end
end
