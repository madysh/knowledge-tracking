class AddRoleToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :role, :tinyint, null: false, default: 0
  end
end
