class CreateAuthTokens < ActiveRecord::Migration[5.1]
  def change
    create_table :auth_tokens do |t|
      t.references :user, null: false, index: true
      t.string :provider, null: false
      t.string :uid, null: false
      t.string :token, null: false
      t.string :avatar_url

      t.timestamps null: false
    end
  end
end
