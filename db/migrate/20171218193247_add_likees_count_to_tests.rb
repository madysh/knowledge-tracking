class AddLikeesCountToTests < ActiveRecord::Migration[5.1]
  def change
    add_column :tests, :likers_count, :integer, default: 0
  end
end
