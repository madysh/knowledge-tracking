class CreateTopics < ActiveRecord::Migration[5.1]
  def change
    create_table :topics do |t|
      t.references :category, null: false, index: true
      t.string :name, null: false
      t.timestamps
    end
  end
end
