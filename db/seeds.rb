if Rails.env.development? && User.admin.empty?
  puts 'Creating admin admin@example.com...'
  admin = User.create!(
    first_name: 'Admin',
    nickname: :admin,
    email: 'admin@example.com',
    password: 'password',
    password_confirmation: 'password',
    role: :admin,
    provider: :local
  )
  admin.confirm
end

unless Category.exists?
  puts 'Creating Categories and Topics...'

  categories_and_topics ={
    ruby: %w[relationships validations],
    html: %w[html5],
    css: %w[css3 sccs],
    js: %w[ES2015 jquery]
  }
  categories_and_topics.each do |category_name, tipics|
    category = Category.create(name: category_name)
    topics_data = tipics.map do |topic_name|
      { name: topic_name, category_id: category.id }
    end
    Topic.create(topics_data)
  end
end

puts 'Done'
