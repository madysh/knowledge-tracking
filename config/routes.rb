Rails.application.routes.draw do
  devise_for :users,
    controllers: {
      omniauth_callbacks: 'omniauth_callbacks',
      registrations: 'registrations'
    }
  ActiveAdmin.routes(self)

  resources :tests do
    resources :test_results, only: %i[new create]
    resources :ratings, only: %i[create update destroy]
    resources :comments, only: :create do
      resources :comments, only: :create
    end
    get '/like', to: 'likes#like'
    get '/unlike', to: 'likes#unlike'
  end
  resources :home, only: :index
  resources :test_results, only: :index

  root to: 'home#index'
end
