Rails.application.config.middleware.use OmniAuth::Builder do
  config = Rails.application.secrets.omniauth

  provider :facebook, config[:facebook][:app_id], config[:facebook][:secret],
    scope: 'email public_profile',
    info_fields: 'email, first_name, last_name',
    image_size: 'normal',
    display: 'popup'

  provider :linkedin, config[:linkedin][:client_id], config[:linkedin][:secret],
    scope: 'r_basicprofile r_emailaddress'

  provider :twitter, config[:twitter][:api_key], config[:twitter][:secret]
end
