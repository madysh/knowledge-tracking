ActiveAdmin.setup do |config|
  config.site_title = 'Knowledge Tracking'
  config.authentication_method = :authenticate_admin_user!
  config.current_user_method = :current_user
  config.logout_link_path = :destroy_user_session_path
  config.batch_actions = true
  config.localize_format = :long
  config.comments_registration_name = 'AdminComment'
  config.comments = false
  config.comments_menu = false
  config.site_title_link = :root
end
