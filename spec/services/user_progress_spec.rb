require 'rails_helper'

RSpec.describe UsersProgress, type: :model do
  let(:current_user) { create :user }
  let(:private_test) do
    create(:test, scope: Test.scopes[:only_for_me])
  end
  let(:public_test) { create(:test) }
  let(:users_public_test) { create(:test, user: current_user) }
  let(:users_private_test) do
    create(:test, scope: Test.scopes[:only_for_me], user: current_user)
  end
  subject { UsersProgress.new(current_user) }

  describe '#count_of_tests' do
    it 'returns 0 when there are no tests' do
      expect(subject.count_of_tests).to eq 0
    end

    it "returns the count of not current user\'s public tests" do
      public_test
      private_test
      users_public_test
      users_private_test
      expect(subject.count_of_tests).to eq 1
    end
  end

  describe '#not_started_tests' do
    it 'returns an empty array when there are no tests' do
      expect(subject.not_started_tests).to eq []
    end

    it 'returns an empty array when there user has no completed tests' do
      test_1 = public_test
      test_2 = create(:test)
      espected_hash = test_data_hash(test_1).merge(test_data_hash(test_2))

      private_test
      users_public_test
      users_private_test

      expect(subject.not_started_tests).to eq espected_hash
    end
  end

  describe '#completed_tests' do
    it 'returns an empty array when there are no tests' do
      expect(subject.completed_tests).to eq []
    end

    it 'returns an empty array when there user has no completed tests' do
      test_1 = create(:test, user: create(:user))
      test_2 = create(:test, user: create(:user))
      test_result_1 = create(:test_result, test: test_1, user: current_user)
      test_result_2 = create(:test_result, test: test_2, user: current_user)
      espected_hash = test_data_hash(
        test_1,
        test_result_1.correct_answers_count
      ).merge(test_data_hash(test_2, test_result_2.correct_answers_count))

      public_test
      private_test
      users_private_test

      expect(subject.completed_tests).to eq espected_hash
    end
  end

  describe '#average_value_of_answers' do
    it 'returns 0 when there are no tests' do
      expect(subject.average_value_of_answers).to eq 0
    end

    it 'returns the average value of of user\'s answers' do
      test_1 = create(:test, user: create(:user))
      test_2 = create(:test, user: create(:user))
      test_result_1 = create(:test_result, test: test_1, user: current_user)
      test_result_1.update_attribute(:correct_answers_count, 0)
      test_result_2 = create(:test_result, test: test_2, user: current_user)
      test_result_2.update_attribute(:correct_answers_count, 5)

      public_test
      private_test
      users_private_test

      expect(subject.average_value_of_answers).to eq 2.5
    end
  end

  private

  def test_data_hash(test, correct_answers_count = nil)
    {
      [test.topic.category.id, test.topic.category.name] => {
        [test.topic.id, test.topic.name] => [
          test_data_attrs(test, correct_answers_count)
        ]
      }
    }
  end

  def test_data_attrs(test, correct_answers_count)
    {
      'id' => nil,
      'cat_id' => test.topic.category.id,
      'cat_name' => test.topic.category.name,
      'topic_id' => test.topic.id,
      'topic_name' => test.topic.name,
      'test_id' => test.id,
      'test_title' => test.title,
      'correct_answers_count' => correct_answers_count
    }
  end
end
