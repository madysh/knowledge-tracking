FactoryBot.define do
  factory :test_result do
    user
    test
    state TestResult.states[:active]
    answers do
      1.upto(Test::QUESTIONS_COUNT).each_with_object({}) do |qi, h|
        h[qi.to_s] = rand(1..Test::ANSWERS_COUNT).to_s
      end
    end
  end
end
