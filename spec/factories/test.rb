FactoryBot.define do
  factory :test do
    user
    topic
    sequence(:title) { |n| "test-#{n}@example.com" }
    description 'description'
    scope Test.scopes[:for_everyone]
    questions do
      1.upto(Test::QUESTIONS_COUNT).each_with_object({}) do |qi, h|
        h[qi.to_s] = {
          body: 'guess the answer',
          'correct_answer': rand(1..Test::ANSWERS_COUNT).to_s,
          'answers': 1.upto(Test::ANSWERS_COUNT).map do |ai|
            { 'body': ai }
          end
        }
      end
    end
  end
end
