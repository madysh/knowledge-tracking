FactoryBot.define do
  factory :user do
    sequence(:email) { |n| "user-#{n}@example.com" }
    sequence(:first_name) { |n| "User#{('a'..'z').to_a.sample * n}" }
    last_name 'Test'
    password 'passwOrd123'
    password_confirmation 'passwOrd123'
    confirmed_at Time.current
  end
end
