require 'rails_helper'

RSpec.describe AuthToken, type: :model do
  it { is_expected.to belong_to(:user) }

  %i[uid token user_id].each do |attr|
    describe "##{attr}" do
      it { is_expected.to validate_presence_of(attr) }
    end
  end

  describe '#provider' do
    context 'validations' do
      it { is_expected.to validate_presence_of(:provider) }
      it do
        is_expected
          .to define_enum_for(:provider)
          .with(%i[facebook linkedin twitter])
      end
    end
  end
end
