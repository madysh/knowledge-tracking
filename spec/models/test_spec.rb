require 'rails_helper'

RSpec.describe Test, type: :model do
  context 'associations' do
    it { is_expected.to belong_to(:topic) }
    it { is_expected.to belong_to(:user) }
  end

  %i[title description user_id topic_id questions].each do |attr_name|
    describe "##{attr_name}" do
      context 'validations' do
        it { is_expected.to validate_presence_of(attr_name) }
      end
    end
  end

  describe '#scope' do
    context 'validations' do
      it { is_expected.to validate_presence_of(:scope) }
      it do
        is_expected
          .to define_enum_for(:scope)
          .with(%i[for_everyone only_for_me])
      end
    end
  end
end
