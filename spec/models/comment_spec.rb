require 'rails_helper'

RSpec.describe Comment, type: :model do
  context 'associations' do
    it { is_expected.to belong_to(:user) }
  end

  %i[user_id commentable_type commentable_id].each do |attr|
    describe "##{attr}" do
      context 'validations' do
        it { is_expected.to validate_presence_of(attr) }
      end
    end
  end

  describe '#body' do
    context 'validations' do
      it { is_expected.to validate_presence_of(:body) }
    end
    it { is_expected.to validate_length_of(:body).is_at_most(256) }
  end
end
