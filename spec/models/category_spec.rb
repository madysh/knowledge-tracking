require 'rails_helper'

RSpec.describe Category, type: :model do
  describe '#name' do
    context 'validations' do
      it { is_expected.to validate_presence_of(:name) }
      it 'should be unique' do
        is_expected.to validate_uniqueness_of(:name).ignoring_case_sensitivity
      end
    end
  end

  it { is_expected.to have_many(:topics).dependent(:destroy) }
end
