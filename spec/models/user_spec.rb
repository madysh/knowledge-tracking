require 'rails_helper'

RSpec.describe User, type: :model do
  describe '#password' do
    context 'validations' do
      it { is_expected.to validate_presence_of(:password).on(:create) }
      it do
        is_expected
          .to validate_length_of(:password)
          .is_at_least(6)
          .is_at_most(64)
      end
    end
  end

  shared_examples 'validates value for name' do |attr|
    it { is_expected.to validate_length_of(:last_name).is_at_most(64) }
    it 'allows values which contains only letters and space' do
      valid_values = %w[AaA aa ivan Rygor Петя Чен\ Ын]
      is_expected.to allow_values(*valid_values).for(attr)
    end
    it 'does not allow values which contains not only letters' do
      invalid_values = %w[1 qwe2 Ivan$ w@ user.1 ivan_p]
      is_expected.to_not allow_values(*invalid_values).for(attr)
    end
  end

  describe '#first_name' do
    context 'validations' do
      it { is_expected.to validate_presence_of(:first_name) }
      it_behaves_like 'validates value for name', :first_name
    end
  end

  describe '#last_name' do
    context 'validations' do
      it_behaves_like 'validates value for name', :last_name
    end
  end

  describe '#nickname' do
    context 'validations' do
      it do
        is_expected
          .to validate_uniqueness_of(:nickname)
          .ignoring_case_sensitivity
      end
      it { is_expected.to validate_length_of(:nickname).is_at_most(64) }
    end
  end

  describe '#email' do
    context 'validations' do
      it do
        is_expected.to validate_uniqueness_of(:email).ignoring_case_sensitivity
      end
      it do
        is_expected.to validate_length_of(:email).is_at_most(64)
      end
      it 'allows email format values' do
        valid_values = %w[user@domain.com -user1-@domain.com]
        is_expected.to allow_values(*valid_values).for(:email)
      end
      it 'does not allow invalid values' do
        invalid_values = %w[
          @@domain.com @domain.com user@com domain.com domain
        ]
        is_expected.to_not allow_values(*invalid_values).for(:email)
      end
    end
  end

  describe '#provider' do
    context 'validations' do
      it { is_expected.to validate_presence_of(:provider) }
      it do
        is_expected
          .to define_enum_for(:provider)
          .with(%i[local facebook linkedin twitter])
      end
    end
  end

  describe '#role' do
    context 'validations' do
      it { is_expected.to validate_presence_of(:role) }
      it { is_expected.to define_enum_for(:role).with(%i[user admin]) }
    end
  end

  context 'associations' do
    it { is_expected.to have_many(:auth_tokens).dependent(:destroy) }
    it { is_expected.to have_many(:tests).dependent(:destroy) }
    it { is_expected.to have_many(:test_results).dependent(:destroy) }
    it { is_expected.to have_many(:ratings).dependent(:destroy) }
    it { is_expected.to have_many(:comments).dependent(:destroy) }
  end
end
