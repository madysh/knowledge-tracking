require 'rails_helper'

RSpec.describe TestResult, type: :model do
  context 'associations' do
    it { is_expected.to belong_to(:test) }
    it { is_expected.to belong_to(:user) }
  end

  %i[user_id test_id answers correct_answers_count].each do |attr_name|
    describe "##{attr_name}" do
      context 'validations' do
        it { is_expected.to validate_presence_of(attr_name) }
      end
    end
  end

  describe '#state' do
    context 'validations' do
      it { is_expected.to validate_presence_of(:state) }
      it { is_expected.to define_enum_for(:state).with(%i[active archived]) }
    end
  end
end
