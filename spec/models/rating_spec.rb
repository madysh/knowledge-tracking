require 'rails_helper'

RSpec.describe Rating, type: :model do
  context 'associations' do
    it { is_expected.to belong_to(:test) }
    it { is_expected.to belong_to(:user) }
  end
end
