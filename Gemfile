source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

gem 'activeadmin'
gem 'acts_as_commentable'
gem 'acts-as-taggable-on', '~> 5.0'
gem 'auto_strip_attributes'
gem 'bootstrap-sass'
gem 'carrierwave', '~> 1.0'
gem 'coffee-rails', '~> 4.2'
gem 'devise', git: 'https://github.com/plataformatec/devise' #, ref: '88e9a85'
gem 'draper'
gem 'haml-rails'
gem 'jbuilder', '~> 2.5'
gem 'jquery-rails'
gem 'kaminari'
gem 'mini_magick'
gem 'omniauth'
gem 'omniauth-facebook'
gem 'omniauth-linkedin-oauth2'
gem 'omniauth-twitter'
gem 'puma', '~> 3.7'
gem 'rails', '~> 5.1.4'
gem 'rails-bootstrap-tabs'
gem 'sass-rails', '~> 5.0'
gem 'simple_form'
gem 'socialization'
gem 'sqlite3'
gem 'turbolinks', '~> 5'
gem 'uglifier', '>= 1.3.0'

group :development, :test do
  gem "factory_bot_rails", "~> 4.0"
  gem 'pry'
  gem 'rspec-rails'
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'rubocop', require: false
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'web-console', '>= 3.3.0'
end

group :test do
  gem 'shoulda-matchers'
end
